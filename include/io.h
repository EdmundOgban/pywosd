#include <stdint.h>
#include <nestegg/nestegg.h>

#ifndef IO_H_
#define IO_H_

int io_read(void *, size_t, void *);
int io_seek(int64_t, int, void *);
int64_t io_tell(void *);
void buffered_io_init(nestegg_io *, void *);

#endif
