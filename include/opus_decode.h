#include <stdint.h>
#include <opus/opus.h>
#include <nestegg/nestegg.h>

#include "richbuffer.h"

#ifndef OPUS_DECODE_H_
#define OPUS_DECODE_H_

OpusDecoder * opus_decoder_open(int32_t, int32_t);
int opus_decode_frame(OpusDecoder *, struct richbuffer *, struct richbuffer *, uint32_t);
void opus_decoder_close(OpusDecoder *);
int opus_alloc_context(OpusDecoder **, struct richbuffer *, nestegg_audio_params *);
char * opus_check_for_errors(int32_t);

#endif
