#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <nestegg/nestegg.h>

#include "io.h"
#include "richbuffer.h"


extern volatile int starving; 


int io_read(void * buffer, size_t length, void * f) {
    struct richbuffer * buf = (struct richbuffer *)f;

    // printf("    ---> io_read called, length:%ld buf->pos:%lu buf->len:%ld\n",
    //     length, buf->pos, buf->len);

    if ((buf->pos + length) > buf->len) {
        length = buf->len - buf->pos;
        starving = 1;
        // printf("    -->> STARVING\n");
    }

    memcpy(buffer, buf->buf + buf->pos, length);
    buf->pos += length;

    if (starving)
        return 0;
    else
        return 1;
    // return fread((char *)buffer, length, 1, (FILE *)f);
}

int io_seek(int64_t offset, int whence, void * f) {
    struct richbuffer * buf = (struct richbuffer *)f;
    int64_t new_position;


    switch (whence) {
        case SEEK_SET:
            new_position = offset;

            if (new_position > buf->len)
                return -1;
        break;

        case SEEK_CUR:
            new_position = buf->pos + offset;

            if (new_position < 0 || new_position > buf->len)
                return -1;                
        break;

        case SEEK_END:
            new_position = buf->len - abs(offset);

            if (new_position < 0)
                return -1;
        break;

        default:
            return -1;
        break;
    }

    // printf("    ---> io_seek called, offset:%ld whence:%d\n", offset, whence);

    buf->pos = new_position;

    return 0;
    // return fseek((FILE *)f, (long)offset, whence);
}

int64_t io_tell(void * f) {
    struct richbuffer * buf = (struct richbuffer *)f;


    return buf->pos;
    // return (int64_t)ftell((FILE *)f);
}

void buffered_io_init(nestegg_io * io, void * f) {
    io->read = io_read;
    io->seek = io_seek;
    io->tell = io_tell;
    io->userdata = f;
}
