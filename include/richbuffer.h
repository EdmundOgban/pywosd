#include <stdint.h>

#ifndef RICHBUFFER_H_
#define RICHBUFFER_H_

struct richbuffer {
    void * buf;
    size_t size;
    size_t len;
    size_t pos;
};

int richbuff_alloc(struct richbuffer *);
int richbuff_realloc(struct richbuffer *);
void richbuff_free(struct richbuffer *);

#endif

