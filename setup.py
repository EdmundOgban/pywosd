from distutils.core import setup, Extension

source_files = "opus_decode.c richbuffer.c io.c pywosd.c" # wosd.c 

module1 = Extension('pywosd',
                    include_dirs = ['include'],
                    libraries = ['opus', 'nestegg'],
                    sources = source_files.split())

setup (name = 'pywosd',
       version = '0.1',
       description = 'A WebM incapsulated Opus stream decoder.',
       ext_modules = [module1])
