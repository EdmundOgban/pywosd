#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <nestegg/nestegg.h>

#include <Python.h>

#include "opus_decode.h"
#include "richbuffer.h"
#include "io.h"

#define CHUNK_SIZE 8192
#define MAX_WEBM_CHUNK_SIZE (CHUNK_SIZE * 2)

#define PY_RAISE_OOM() PyErr_SetString(PyExc_MemoryError, "Out of Memory.")


nestegg_io io;
nestegg * demux_ctx;
OpusDecoder * opusd_ctx;
PyObject * PyExc_OpusDecode;

volatile int64_t realign_pos;
volatile int starving;

struct richbuffer webm_data;
struct richbuffer opus_data;
struct richbuffer pcm_data_a;
struct richbuffer pcm_data_b;
struct richbuffer * pcm_data;

// Forward declaration
static PyObject * module_destroy(PyObject * self, PyObject * args);


static PyObject * module_ioread(PyObject * self, PyObject * args) {
    char * buffer;
    size_t length;
    int ret;
    PyObject * pyret;


    if (!PyArg_ParseTuple(args, "n", &length))
        return NULL;

    buffer = malloc(length);
    if (buffer == NULL) {
        PY_RAISE_OOM();
        return NULL;
    }

    ret = io_read(buffer, length, &webm_data);

    if (ret == 0)
        pyret = Py_BuildValue("y", "");
    else
        pyret = Py_BuildValue("y#", buffer, length);

    free(buffer);

    return pyret;
}

static PyObject * module_ioseek(PyObject * self, PyObject * args) {
    int64_t offset;
    int whence;
    int ret;


    if (!PyArg_ParseTuple(args, "Li", &offset, &whence))
        return NULL;

    if (!(SEEK_SET <= whence && whence <= SEEK_END)) {
        PyErr_SetString(PyExc_ValueError, "Whence argument must be 0 <= arg <= 2.");
        return NULL;
    }

    ret = io_seek(offset, whence, &webm_data);

    return Py_BuildValue("i", ret);
}

static PyObject * module_iotell(PyObject * self, PyObject * args) {
    int64_t ret;


    ret = io_tell(&webm_data);

    return Py_BuildValue("L", ret);
}

static PyObject * module_feed_chunk(PyObject * self, PyObject * args) {
    Py_buffer pybuf;


    if (!PyArg_ParseTuple(args, "y*", &pybuf))
        return NULL;

    if (webm_data.buf == NULL) {
        webm_data.size = MAX_WEBM_CHUNK_SIZE;

        if (!richbuff_alloc(&webm_data)) {
            PY_RAISE_OOM();
            PyBuffer_Release(&pybuf);
            return NULL;
        }
    }

    memcpy(webm_data.buf + webm_data.len, pybuf.buf, pybuf.len);
    webm_data.len += pybuf.len;
    PyBuffer_Release(&pybuf);

    if (demux_ctx == NULL) {
        buffered_io_init(&io, &webm_data);

        if (nestegg_init(&demux_ctx, io, NULL, -1) != 0) {
            /* An initialization failure could mean that we don't have enough 
             * header data, so we ask for more and try again, using
             * MAX_WEBM_CHUNK_SIZE as a hard limit.
             */
            if (starving && webm_data.len < MAX_WEBM_CHUNK_SIZE) {
                io_seek(0, SEEK_SET, &webm_data);
                starving = 0;

                Py_RETURN_FALSE;
            }
            else {
                PyErr_SetString(PyExc_RuntimeError,
                    "Cannot allocate nestegg context.");
                richbuff_free(&webm_data);

                return NULL;
            }
        }
    }

    starving = 0;

    Py_RETURN_TRUE;
}

static char * webm_decode_chunks(nestegg_packet * pkt,
                                 uint32_t chunks,
                                 nestegg_audio_params * ap,
                                 uint32_t samples_per_channel) {
    int32_t decoded_samples = 0;
    char * opus_err;


    for (uint32_t chunk = 0; chunk < chunks; chunk++) {
        nestegg_packet_data(
            pkt, chunk, (unsigned char **)&opus_data.buf, &opus_data.size);
        decoded_samples = opus_decode_frame(
            opusd_ctx, &opus_data, pcm_data, samples_per_channel);

        if ((opus_err = opus_check_for_errors(decoded_samples)) != NULL)
            return opus_err;

        pcm_data->len += decoded_samples * ap->channels * ap->depth / 8;
    }

    return NULL;
}

#define PCM_CONVERT_TEMPLATE(dst_buf, src_len, dst_len, sample) \
do {                                                            \
    for (size_t i = 0; i < src_len; sample++, i += 2) {         \
        dst_buf[i] = *sample;                                   \
        dst_buf[i+1] = *sample;                                 \
    }                                                           \
                                                                \
    *dst_len = src_len * 2;                                     \
} while (0)

static inline void pcm_convert_m8_to_s8(struct richbuffer * src,
                                        struct richbuffer * dst) {
    int8_t * sample = (int8_t *)src->buf;
    int8_t * dst_buf = (int8_t *)dst->buf;

    PCM_CONVERT_TEMPLATE(dst_buf, src->len, &dst->len, sample);
}

static inline void pcm_convert_m16_to_s16(struct richbuffer * src,
                                          struct richbuffer * dst) {
    int16_t * sample = (int16_t *)src->buf;
    int16_t * dst_buf = (int16_t *)dst->buf;

    PCM_CONVERT_TEMPLATE(dst_buf, src->len, &dst->len, sample);
}

static inline int pcm_convert_m_to_s(struct richbuffer * pcm_data,
                                     struct richbuffer * pcm_data_a,
                                     struct richbuffer * pcm_data_b,
                                     unsigned int bits_per_sample) {

    struct richbuffer * src;
    struct richbuffer * dst;

    if (pcm_data == pcm_data_a) {
        src = pcm_data_a;
        dst = pcm_data_b;
    }
    else {
        src = pcm_data_b;
        dst = pcm_data_a;
    }

    switch (bits_per_sample) {
        case 8:
            pcm_convert_m8_to_s8(src, dst);
            return 0;
        break;
        case 16:
            pcm_convert_m16_to_s16(src, dst);
            return 0;
        break;
        default:
            return -1;
        break;
    }
}

static inline void pcm_swap_buffers(struct richbuffer ** pcm_data,
                                    struct richbuffer * pcm_data_a,
                                    struct richbuffer * pcm_data_b) {
    *pcm_data = (*pcm_data == pcm_data_a) ? pcm_data_b : pcm_data_a;
}

static PyObject * module_next_packet(PyObject * self, PyObject * args) {
    uint32_t track;
    uint32_t chunks;

    static uint32_t samples_per_channel = 0;
    static uint32_t last_chunks = 1;
    static uint32_t pcm_chunk_size;
    static nestegg_audio_params ap;

    nestegg_packet * pkt;
    PyObject * pyret;


    if (demux_ctx == NULL) {
        PyErr_SetString(PyExc_RuntimeError,
            "Function called without a valid context.");
        return NULL;
    }

    realign_pos = io_tell(&webm_data);

    if (nestegg_read_packet(demux_ctx, &pkt) <= 0) {
        io_seek(0, SEEK_SET, &webm_data);
        webm_data.len -= realign_pos;

        memmove(webm_data.buf, webm_data.buf + realign_pos, webm_data.len);
        nestegg_read_reset(demux_ctx);

        return Py_BuildValue("(iy#)",
            starving, pcm_data->buf, pcm_data->len);
    }

    nestegg_packet_track(pkt, &track);

    // Decode the first track only
    if (track == 0) {
        nestegg_packet_count(pkt, &chunks);

        if (opusd_ctx == NULL) {
            nestegg_track_audio_params(demux_ctx, track, &ap);

            // Calculating the needed space for the maximum frame size of 120ms
            samples_per_channel = (uint32_t)ap.rate * ap.depth / 8 * 0.12;
            // Always allocate enough space for two channels.
            // In case of monaural content, we take care of the conversion later on
            pcm_chunk_size = samples_per_channel * 2;
            pcm_data_a.size = pcm_chunk_size * chunks;
            pcm_data_b.size = pcm_chunk_size * chunks;
            pcm_data = &pcm_data_a;

            if (opus_alloc_context(&opusd_ctx, pcm_data, &ap) != 0) {
                PY_RAISE_OOM();
                goto exc_exit;
            }
        }

        // Enlarge PCM buffers if we're going to decode more chunks in a row
        if (chunks > last_chunks) {
            pcm_data_a.size = pcm_chunk_size * chunks;
            pcm_data_b.size = pcm_chunk_size * chunks;

            if (!richbuff_realloc(&pcm_data_a)) {
                PY_RAISE_OOM();
                goto exc_exit;
            }

            // Realloc PCM buffer B only if it has been used
            if (pcm_data_b.buf != NULL) {
                if (!richbuff_realloc(&pcm_data_b)) {
                    PY_RAISE_OOM();
                    goto exc_exit;
                }
            }

            last_chunks = chunks;
        }

        // Decode each chunk of data.
        char * opus_err = webm_decode_chunks(pkt, chunks, &ap, samples_per_channel);

        if (opus_err != NULL) {
            PyErr_Format(PyExc_OpusDecode, "Opus decode failed: %s", opus_err);
            goto exc_exit;
        }
    }

    if (ap.channels == 1) {
        int ret;

        if (pcm_data_b.buf == NULL) {
            if (!richbuff_alloc(&pcm_data_b)) {
                PY_RAISE_OOM();
                goto exc_exit;
            }
        }

        ret = pcm_convert_m_to_s(pcm_data, &pcm_data_a, &pcm_data_b, ap.depth);

        if (ret == -1) {
            PyErr_Format(PyExc_NotImplementedError,
                "Unhandled PCM sample size: M%d", ap.depth);
            goto exc_exit;
        }

        pcm_swap_buffers(&pcm_data, &pcm_data_a, &pcm_data_b);
    }

    nestegg_free_packet(pkt);
    pyret = Py_BuildValue("(iy#)", starving, pcm_data->buf, pcm_data->len);
    pcm_data->len = 0;

    return pyret;

exc_exit:
    nestegg_free_packet(pkt);
    module_destroy(self, args);

    return NULL;
}

static PyObject * module_destroy(PyObject * self, PyObject * args) {
    puts("    ---- module_destroy called");

    if (opusd_ctx != NULL)
        opus_decoder_close(opusd_ctx);

    if (demux_ctx != NULL)
        nestegg_destroy(demux_ctx);

    if (pcm_data_a.buf != NULL)
        richbuff_free(&pcm_data_a);

    if (pcm_data_b.buf != NULL)
        richbuff_free(&pcm_data_b);

    if (webm_data.buf != NULL)
        richbuff_free(&webm_data);

    opusd_ctx = NULL;
    demux_ctx = NULL;

    Py_RETURN_NONE;
}

// Method definition object for this extension, these argumens mean:
// ml_name: The name of the method
// ml_meth: Function pointer to the method implementation
// ml_flags: Flags indicating special features of this method, such as
//          accepting arguments, accepting keyword arguments, being a
//          class method, or being a static method of a class.
// ml_doc:  Contents of this method's docstring
static PyMethodDef module_methods[] = { 
    {   
        "destroy", module_destroy, METH_NOARGS,
        "Deallocates decoding engine."
    },
    {   
        "feed_chunk", module_feed_chunk, METH_VARARGS,
        "Feeds chunked WebM data."
    },
    {   
        "next_packet", module_next_packet, METH_NOARGS,
        "Gets audio data per chunk."
    },
    {
        "ioread", module_ioread, METH_VARARGS,
        "Reads the underlying WebM file-like buffer."
    },
    {   
        "ioseek", module_ioseek, METH_VARARGS,
        "Seeks the underlying WebM file-like buffer."
    },
    {   
        "iotell", module_iotell, METH_VARARGS,
        "Tells the position of the underlying WebM file-like buffer."
    },
    {NULL, NULL, 0, NULL}
};


// Module definition
// The arguments of this structure tell Python what to call your extension,
// what it's methods are and where to look for it's method definitions
static struct PyModuleDef module_definition = { 
    PyModuleDef_HEAD_INIT,
    "pywosd",
    "A WebM incapsulated Opus stream decoder.",
    -1,
    module_methods
};

// Module initialization
// Python calls this function when importing your extension. It is important
// that this function is named PyInit_[[your_module_name]] exactly, and matches
// the name keyword argument in setup.py's setup() call.
PyMODINIT_FUNC PyInit_pywosd(void) {
    PyObject * module;
    Py_Initialize();
    
    module = PyModule_Create(&module_definition);
    PyExc_OpusDecode = PyErr_NewException("pywosd.OpusDecodeException", NULL, NULL);

    PyModule_AddObject(module, "OpusDecodeException", PyExc_OpusDecode);

    return module;
}