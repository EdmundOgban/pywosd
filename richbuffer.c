#include <stdlib.h>
#include <stdio.h>

#include "richbuffer.h"


int richbuff_alloc(struct richbuffer * buf) {
    buf->buf = malloc(buf->size);

    if (buf->buf == NULL)
        return 0;

    // fprintf(stderr, "    ---- alloc'ed %ld bytes\n", buf->size);
    buf->len = 0;
    buf->pos = 0;

    return 1;
}

int richbuff_realloc(struct richbuffer * buf) {
    void * new_buf;


    new_buf = realloc(buf->buf, buf->size);

    if (new_buf == NULL) {
        free(buf->buf);
        return 0;
    }

    buf->buf = new_buf;

    // fprintf(stderr, "    ---- realloc'ed %ld bytes\n", buf->size);
    return 1;
}

void richbuff_free(struct richbuffer * buf) {
    free(buf->buf);

    buf->buf = NULL;
}
