#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "opus_decode.h"


int opus_alloc_context(OpusDecoder ** ctx, struct richbuffer * pcm_data,
                       nestegg_audio_params * ap) {
    if (!richbuff_alloc(pcm_data))
        return -1;

    // TODO: Check effective opus context allocation
    *ctx = opus_decoder_open((int32_t)ap->rate, ap->channels);

    if (*ctx == NULL)
        return -1;

    return 0;
}

OpusDecoder * opus_decoder_open(int32_t srate, int32_t channels) {
    int opus_err = 0;
    OpusDecoder * opusd_ctx;


    fprintf(stderr, "Creating Opus decoder (srate:%d channels:%d)\n", srate, channels);
    opusd_ctx = opus_decoder_create(srate, channels, &opus_err);

    if (!opus_err)
        return opusd_ctx;

    return NULL;
}

int opus_decode_frame(OpusDecoder * opusd_ctx, struct richbuffer * inbuf,
                      struct richbuffer * outbuf, uint32_t samples_per_channel) {
    // fprintf(stderr, "opus_decode inbuf->size %lu outbuf->size %lu outbuf->len "
    //     "%lu samples_per_channel %lu\n",
    //     inbuf->size, outbuf->size, outbuf->len, samples_per_channel);

    return opus_decode(opusd_ctx, inbuf->buf, inbuf->size,
        outbuf->buf + outbuf->len, samples_per_channel, 0);
}

void opus_decoder_close(OpusDecoder * opusd_ctx) {
    opus_decoder_destroy(opusd_ctx);
}

char * opus_check_for_errors(int32_t retcode) {
    static char str_err[24];

    switch (retcode) {
        case OPUS_BAD_ARG:
            strcpy(str_err, "OPUS_BAD_ARG");
        break;

        case OPUS_BUFFER_TOO_SMALL:
            strcpy(str_err, "OPUS_BUFFER_TOO_SMALL");
        break;

        case OPUS_INTERNAL_ERROR:
            strcpy(str_err, "OPUS_INTERNAL_ERROR");
        break;

        case OPUS_INVALID_PACKET:
            strcpy(str_err, "OPUS_INVALID_PACKET");
        break;

        case OPUS_UNIMPLEMENTED:
            strcpy(str_err, "OPUS_UNIMPLEMENTED");
        break;

        case OPUS_INVALID_STATE:
            strcpy(str_err, "OPUS_INVALID_STATE");
        break;

        case OPUS_ALLOC_FAIL:
            strcpy(str_err, "OPUS_ALLOC_FAIL");
        break;

        default:
            return NULL;
        break;
    }

    return str_err;
}
